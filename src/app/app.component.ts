import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  title = 'ngpwa';
  todos;

  constructor(
    private dataService: DataService
  ) {}

  ngOnInit() {
    this.dataService.getData().pipe(first()).subscribe((todos)  => this.todos = todos);
  }
}
